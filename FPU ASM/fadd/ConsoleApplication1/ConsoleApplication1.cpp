#include "stdafx.h"
#include <cstdlib>
#include <iostream>

using namespace std;

extern "C" float sum(float f1, float f2);

int main() {
	float f1, f2;

	//while (true) {
		printf_s("\nEnter float 1: "); scanf_s("%f", &f1);
		printf_s("\nEnter float 2: "); scanf_s("%f", &f2);

		//_asm {
		//	finit; ������������ FPU
		//	fld DWORD PTR f1; ���������� ����� � ������� ����� ������������
		//	fadd DWORD PTR f2; ���������� ����� � ������� ������������ � f2
		//	fstp DWORD PTR fsum; ������ ����� � fsum � ����������� �������� ����� FPU
		//	fwait; ������������� CPU �� ��������� ����������
		//}

		printf_s("f1 + f2 = %6.3f\n", sum(f1, f2));
	//}

	system("pause");
    return 0;
}

