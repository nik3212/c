.686
.model flat
	public _sum
.code
_sum proc 
	push EBP 
	mov EBP, ESP

	finit
	fld DWORD PTR [EBP+8]
	fadd DWORD PTR [EBP+12]
	fwait

	pop EBP
	ret
_sum endp
end