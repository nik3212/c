#pragma once

#ifndef STACK_H_
#define STACK_H_

typedef unsigned long Item;

class Stack {
private: 
	enum {MAX = 10};
	Item items[MAX]; // ������ �������� �������� �����
	int top; // ������ ������� �����
public:
	Stack();
	bool isEmpty() const;
	bool isFull() const;
	// push () ���������� false, ���� ���� �����, � true - � ��������� ������
	bool push(const Item & item);
	// pop() ���������� false(), ���� ���� ����, � true - � ��������� ������
	bool pop(Item & item);
};
#endif